/// <reference no-default-lib="true" />
/// <reference lib="dom" />
/// <reference lib="dom.iterable" />
/// <reference lib="dom.asynciterable" />
/// <reference lib="deno.ns" />

import "$std/dotenv/load.ts";

import { start } from "$fresh/server.ts";
import manifest from "./fresh.gen.ts";
import config, { wordfreshConfig } from "./fresh.config.ts";
import { createWordfresh } from "../wordfresh/server/mod.ts";

createWordfresh(wordfreshConfig);

await start(manifest, config);
