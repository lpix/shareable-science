import { useEffect, useRef } from "preact/hooks";

export const useIsMounted = () => {
  const isMounted = useRef(true);

  useEffect(() => {
    isMounted.current = true;

    return () => {
      isMounted.current = false;
    };
  });

  return isMounted.current;
};

const uids = (n: number) => {
  return Array(n)
    .fill(null)
    .map(() => Math.random().toString(16).slice(2));
};

export const useStableIds = (numNeeded = 1): string[] => {
  const stableIds = useRef<string[]>(uids(numNeeded));
  const extra = numNeeded - stableIds.current.length;
  if (extra > 0) {
    stableIds.current = stableIds.current.concat(uids(extra));
  }

  return stableIds.current;
};
