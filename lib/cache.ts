import { HttpError } from "./http.ts";

interface Options {
  invalidateAfter: number;
  checkInterval: number;
}

const defOptions: Options = {
  invalidateAfter: 60 * 60 * 1000,
  checkInterval: 60 * 60 * 1000,
};

type TCache<TRes> = Record<
  string,
  { createdAt: number; data: TRes }
>;

const createCache = <TUid extends string | number, TRes>(
  options?: Partial<Options>,
) => {
  const { invalidateAfter, checkInterval } = {
    ...defOptions,
    ...options,
  };

  const cache = {} as TCache<TRes>;
  setInterval(() => {
    const now = Date.now();
    Object.keys(cache).forEach((uid) => {
      if ((now - cache[uid].createdAt) > invalidateAfter) {
        delete cache[uid];
      }
    });
  }, checkInterval);

  return async (uid: TUid, request: Request): Promise<TRes> => {
    const suid = String(uid);
    const now = Date.now();
    const entry = cache[suid];

    if (!entry || (now - entry.createdAt) > invalidateAfter) {
      const res = await fetch(request);
      if (res.status < 400) {
        const data = await res.json();
        cache[suid] = { createdAt: now, data };
        return data;
      } else {
        if (entry) {
          delete cache[suid];
        }
        throw new HttpError(res.status);
      }
    }

    return cache[suid].data;
  };
};

export default createCache;
