export class HttpError extends Error {
  public status: number;
  public userFacingMessage?: string;
  public isHttpError: true;

  constructor(status: number, userFacingMessage?: string) {
    super(status.toString());
    this.status = status;
    this.userFacingMessage = userFacingMessage;
    this.isHttpError = true;
  }
}

export const isHttpError = (input: HttpError | unknown): input is HttpError => {
  return !!(input as HttpError)?.isHttpError;
};

export const respondWithError = (err: HttpError | number) => {
  let status: number;
  let userFacingMessage: string | undefined;

  if (typeof err === "number") {
    status = err;
  } else {
    status = err.status;
    userFacingMessage = err.userFacingMessage;
  }

  return new Response(JSON.stringify({ ok: false, userFacingMessage }), {
    status,
  });
};
