import { FreshContext } from "$fresh/server.ts";
import { parse } from "querystring";
import { z } from "zod";

export const queryParams = <Q>(schema: z.Schema<Q>) => (url: string) => {
  const params = parse(new URL(url).search, { arrayFormat: "comma" });
  const result = schema.safeParse(params);
  return result;
};

export const params =
  <Q>(schema: z.Schema<Q>) => (params: Record<string, string>) => {
    const result = schema.safeParse(params);
    return result;
  };

export const getUid = (ctx: FreshContext) => {
  try {
    const ctxUid = ctx.params.uid;
    z.string().parse(ctxUid);
    return ctxUid;
  } catch (_err) {
    return null;
  }
};
