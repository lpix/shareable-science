const API_BASE = "https://api.unpaywall.org/v2";
const USER_EMAIL = "casey@livingpixel.io";
import type { Citation, OaLocation } from "./types.ts";

export const getOaLocations = async (doi: string): Promise<OaLocation[]> => {
  try {
    const res = await fetch(
      new Request(`${API_BASE}/${doi}?email=${USER_EMAIL}`, {
        method: "GET",
      }),
    );

    const results = await res.json();

    if (!results.oa_locations) return [];

    // deno-lint-ignore no-explicit-any
    return results.oa_locations.map((location: any) => ({
      url: location.url,
      label: location.label,
    }));
  } catch (err) {
    console.error(err);
    return [];
  }
};

export const getAllLocations = (
  citation: Citation,
  oaLocations: OaLocation[],
) => {
  let allLocations: OaLocation[] = [{
    url: `https://pubmed.ncbi.nlm.nih.gov/${citation.pmid}/`,
    label: "PubMed",
  }];

  if (citation.pmc) {
    allLocations = [...allLocations, {
      url: `https://www.ncbi.nlm.nih.gov/pmc/articles/PMC${citation.pmc}/`,
      label: "PubMed Central",
    }];
  }

  if (citation.doi) {
    allLocations = [...allLocations, {
      url: `https://doi.org/${citation.doi}/`,
      label: "DOI.org",
    }];
  }

  return [...allLocations, ...oaLocations];
};
