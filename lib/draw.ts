import { useCallback, useEffect, useRef, useState } from "preact/hooks";
import { useFontLoader } from "./fonts.ts";
import type { Citation, PreviewTextStyle } from "./types.ts";

interface CanvasInfo {
  el: HTMLCanvasElement;
  width: number;
  height: number;
  ctx: CanvasRenderingContext2D;
}

const FONT_SIZE_LG = 24;
const FONT_SIZE_SM = 14;

const useCanvas = (
  citation: Citation,
  updateTime: number,
  style: PreviewTextStyle,
  image?: HTMLCanvasElement | null,
) => {
  const canvasInfo = useRef<CanvasInfo>();
  const [citationHeight, setCitationHeight] = useState<number>(0);
  const isFontLoaded = useFontLoader(style.font);

  const drawBackground = (citationHeight: number) => {
    if (!canvasInfo.current) return;
    const { ctx, width, height } = canvasInfo.current;

    if (image) {
      ctx.imageSmoothingEnabled = false;
      ctx.drawImage(image, 0, 0, width, height);
    }

    ctx.beginPath();
    ctx.rect(0, 0, width, citationHeight);
    ctx.fillStyle = style.darkBg ? "#333" : "#fff";
    ctx.globalAlpha = style.opacity;
    ctx.fill();
    ctx.globalAlpha = 1;
  };

  const drawCitation = () => {
    if (!canvasInfo.current) return;
    const { ctx, width } = canvasInfo.current;

    ctx.fillStyle = style.darkBg ? "#fff" : "#333";
    let yCursor = FONT_SIZE_SM + 6;

    // write journal
    ctx.font = `700 ${FONT_SIZE_SM}px "${style.font.value}"`;
    ctx.fillText(citation.journal, 10, yCursor);

    // // write year
    const yearWidth = ctx.measureText(String(citation.year)).width;
    ctx.fillText(String(citation.year), width - 10 - yearWidth, yCursor);

    // // write title line
    ctx.font = `400 ${FONT_SIZE_LG}px "${style.font.value}"`;
    yCursor += 2;
    wrapText(ctx, citation.title, width - 20).forEach(
      (line) => {
        yCursor += FONT_SIZE_LG;
        ctx.fillText(line, 10, yCursor);
      },
    );

    yCursor += FONT_SIZE_SM + 6;

    // write authors line
    ctx.font = `400 ${FONT_SIZE_SM}px "${style.font.value}"`;
    ctx.measureText(citation.authors).width > width - 20
      ? ctx.fillText(reduceAuthors(citation.authors), 10, yCursor)
      : ctx.fillText(citation.authors, 10, yCursor);

    setCitationHeight(yCursor + 10);
  };

  const onRender = useCallback((el: HTMLCanvasElement | null) => {
    if (!el) return;

    canvasInfo.current = {
      el,
      ctx: el.getContext("2d") as CanvasRenderingContext2D,
      width: el.clientWidth,
      height: el.clientHeight,
    };

    drawCitation();
  }, []);

  useEffect(() => {
    if (!canvasInfo.current) return;
    const { ctx, width, height } = canvasInfo.current;

    ctx.clearRect(0, 0, width, height);

    drawBackground(citationHeight);
    drawCitation();
  }, [updateTime, citationHeight, style, isFontLoaded]);

  const getCanvasBlob = useCallback<() => Promise<Blob>>(() => {
    return new Promise((resolve, reject) => {
      if (!canvasInfo.current?.el) {
        reject(new Error("NoCanvasElement"));
        return;
      }

      canvasInfo.current.el.toBlob((blob) => {
        if (!blob) {
          return reject(new Error("NoCanvasBlob"));
        }
        resolve(blob);
      });
    });
  }, []);

  return { onRender, getCanvasBlob };
};

export default useCanvas;

function wrapText(
  ctx: CanvasRenderingContext2D,
  text: string,
  maxWidth: number,
) {
  const words = text.split(" ");
  let lines: string[] = [];
  let currentLine = words[0];

  for (let i = 1; i < words.length; i++) {
    const word = words[i];
    const width = ctx.measureText(currentLine + " " + word).width;
    if (width < maxWidth) {
      currentLine += " " + word;
    } else {
      lines = lines.concat(currentLine);
      currentLine = word;
    }
  }
  lines = lines.concat(currentLine);
  return lines;
}

function reduceAuthors(authors: string) {
  const authArray = authors.split(",");
  if (authArray.length < 4) return authors;
  return `${authArray[0]}, ${authArray[1]} [\u2026] ${
    authArray[authArray.length - 1]
  }`;
}
