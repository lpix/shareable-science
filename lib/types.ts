export interface Citation {
  pmid: number;
  year: number;
  title: string;
  authors: string;
  journal: string;
  doi?: string;
  pmc?: number;
}

export interface OaLocation {
  url: string;
  label?: string;
}

export interface Font {
  label: string;
  value: string;
  url: string;
}

export interface PreviewTextStyle {
  font: Font;
  opacity: number;
  darkBg: boolean;
}
