import { JSX } from "preact/jsx-runtime";
import Cropper from "cropperjs";

import { useCallback, useEffect, useRef } from "preact/hooks";
import { forwardRef, MutableRefObject } from "preact/compat";

export interface Props {
  src: string;
  debounceInterval: number;
  onUpdate: (timestamp: number) => void;
  onLoad?: JSX.DOMAttributes<HTMLImageElement>["onLoad"];
  onError?: JSX.DOMAttributes<HTMLImageElement>["onError"];
}

const PreactCropper = forwardRef<HTMLCanvasElement, Props>((
  { src, onUpdate, debounceInterval, onLoad, onError },
  ref,
) => {
  // deno-lint-ignore no-explicit-any
  const cropper = useRef<any>();
  const updateTimer = useRef<number>();

  const onRender = useCallback((el: HTMLImageElement | null) => {
    if (!el && cropper.current) {
      cropper.current.destroy();
    }

    cropper.current = new Cropper(el, {
      aspectRatio: 16 / 9,
      zoomOnWheel: false,
      crop: () => {
        clearInterval(updateTimer.current);

        const canvas = cropper.current.getCroppedCanvas({
          width: 600,
          height: 335,
        }) as HTMLCanvasElement;

        if (ref) {
          const _ref = ref as MutableRefObject<HTMLCanvasElement>;
          _ref.current = canvas;
        }
        setTimeout(() => {
          onUpdate(Date.now());
        }, debounceInterval);
      },
    });
  }, []);

  useEffect(() => {
    if (cropper.current) {
      cropper.current.replace(src, false);
    }
  }, [src]);

  return (
    <div>
      <img
        src={src}
        ref={onRender}
        onError={onError}
        onLoad={onLoad}
      />
    </div>
  );
});

export default PreactCropper;
