import { DOMParser } from "deno-dom";
import { datetime } from "ptera";

import createCache from "./cache.ts";
import { Citation } from "./types.ts";
import { HttpError } from "./http.ts";

export const API_BASE = "https://endymion.caydenberg.io/pubmed";
export const ACCESS_KEY = "cashbox-impure-water";
export const SEARCH_DEBOUNCE = 1000;

export const getFigureUrls = async (pmid: number): Promise<string[]> => {
  const res = await fetch(`https://pubmed.ncbi.nlm.nih.gov/${pmid}`);

  if (res.status >= 400) {
    return [];
  }

  const text = await res.text();
  const doc = new DOMParser().parseFromString(text, "text/html");
  if (!doc) {
    return [];
  }

  const images = doc.querySelectorAll("a.figure-link");

  const urls = Array.from(images).map((el) =>
    (el as unknown as HTMLAnchorElement).getAttribute("href")
  ).filter(Boolean) as string[];

  return urls;
};

interface NodeNcbiSearchResult {
  papers: {
    raw: Record<string, unknown>;
    pubDate: string;
    title: string;
    authors: string;
    pmid: number;
    pmc?: number;
    doi?: string;
  }[];
  count: number;
}

const processPaper = (paper: NodeNcbiSearchResult["papers"][number]) => ({
  pmid: paper.pmid,
  year: datetime(paper.pubDate).year,
  title: paper.title,
  authors: paper.authors,
  journal: (paper.raw.source as string) || "",
  doi: paper.doi,
  pmc: paper.pmc,
});

const fetchSearch = createCache<string, NodeNcbiSearchResult>();
export const doSearch = async (
  query: string,
  page = 0,
): Promise<{
  data: Citation[] | null;
  count: number;
  error?: HttpError | Error | unknown;
}> => {
  try {
    const uid = `${encodeURIComponent(query)}/${page}`;
    const data = await fetchSearch(
      uid,
      new Request(`${API_BASE}/search/${query}${page ? `?page=${page}` : ""}`, {
        method: "GET",
        headers: {
          "ACCESS-KEY": ACCESS_KEY,
        },
      }),
    );

    return {
      data: data.papers.map((paper) => processPaper(paper)),
      count: data.count,
    };
  } catch (error: HttpError | Error | unknown) {
    return {
      data: null,
      count: 0,
      error,
    };
  }
};

const fetchSummary = createCache<
  number,
  NodeNcbiSearchResult["papers"][number]
>();
export const getSummary = async (pmid: number): Promise<{
  data: ReturnType<typeof processPaper> | null;
}> => {
  const res = await fetchSummary(
    pmid,
    new Request(`${API_BASE}/summary/${pmid}`, {
      method: "GET",
      headers: {
        "ACCESS-KEY": ACCESS_KEY,
      },
    }),
  );

  return {
    data: processPaper(res),
  };
};

export const getPaperData = (pmid: number) => {
  return Promise.all([getSummary(pmid), getFigureUrls(pmid).catch(() => [])])
    .then(
      ([summaryRes, figureRes]) => {
        if (!summaryRes.data) {
          throw new Error("ErrorFromNCBI");
        }
        return {
          citation: summaryRes.data,
          figureUrls: figureRes,
        };
      },
    );
};
