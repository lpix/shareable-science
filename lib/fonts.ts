import { useEffect, useState } from "preact/hooks";
import type { Font } from "./types.ts";

export const LOADABLE_FONTS: Font[] = [
  {
    label: "Rubik",
    value: "Rubik",
    url:
      "url(https://cdn.jsdelivr.net/fontsource/fonts/rubik:vf@latest/latin-wght-normal.woff2)",
  },
  {
    label: "System Sans Serif",
    value: "sans-serif",
    url: "",
  },
  {
    label: "Roboto",
    value: "Roboto",
    url:
      "url(https://cdn.jsdelivr.net/fontsource/fonts/roboto@latest/latin-400-normal.woff2)",
  },
  {
    label: "Open Sans",
    value: "Open Sans",
    url:
      "url(https://cdn.jsdelivr.net/fontsource/fonts/open-sans:vf@latest/latin-wght-normal.woff2)",
  },
  {
    label: "Fira Sans",
    value: "Fira Sans",
    url:
      "url(https://cdn.jsdelivr.net/fontsource/fonts/fira-sans@latest/latin-400-normal.woff2)",
  },
  {
    label: "Lato",
    value: "Lato",
    url:
      "url(https://cdn.jsdelivr.net/fontsource/fonts/lato@latest/latin-400-normal.woff2)",
  },
];

export const addFont = (font: FontFace) => {
  document.fonts.add(font);
};

/**
 * WARNING: does not work as you might expect in some browsers. Use with caution.
 * See https://bugzilla.mozilla.org/show_bug.cgi?id=1252821
 */
export const checkFont = (font: string) => {
  return document.fonts.check(`700 14px ${font}`);
};

export const loadFont = async (name: string, url: string) => {
  const font = new FontFace(name, url);

  addFont(font);

  await font.load();
};

export const useFontLoader = (currentFont: Font) => {
  const [loadedFonts, setLoadedFonts] = useState<string[]>([]);
  useEffect(() => {
    const initiallyAvailableFonts = LOADABLE_FONTS.filter((font) => {
      if (!font.url) return true;
      if (checkFont(font.value)) return true;
      return false;
    });

    setLoadedFonts(initiallyAvailableFonts.map((font) => font.value));
  }, []);

  useEffect(() => {
    if (loadedFonts.includes(currentFont.value)) return;
    if (checkFont(currentFont.value)) return;

    loadFont(currentFont.value, currentFont.url).then(() => {
      setLoadedFonts((init) => init.concat(currentFont.value));
    });
  }, [currentFont.value]);

  return loadedFonts.includes(currentFont.value);
};
