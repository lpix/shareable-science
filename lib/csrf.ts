import { create, verify } from "djwt";

interface Payload {
  pmid: number;
}

const key = await crypto.subtle.generateKey(
  { name: "HMAC", hash: "SHA-512" },
  true,
  ["sign", "verify"],
);

export const createJwt = (pmid: number) => {
  return create({ alg: "HS512", typ: "JWT" }, { pmid }, key);
};

export const verifyJwt = (jwt: string): Promise<Payload | null> => {
  return verify(jwt, key).then((payload) => {
    const _payload = payload as unknown as Payload;
    return _payload;
  }).catch(() => {
    return null;
  });
};
