import { defineConfig } from "$fresh/server.ts";
import tailwind from "$fresh/plugins/tailwind.ts";

import { Config } from "wordfresh";
import { Share } from "./models/Shares.ts";

export default defineConfig({
  plugins: [tailwind()],
});

export const wordfreshConfig: Config = {
  dbBaseUrl: (Deno.env.get("DB_BASE_URL") as string) ||
    "http://admin:admin@localhost:5984",
  siteUrl: (Deno.env.get("SITE_URL") as string) || "http://localhost:8000",

  dbName: "shareable-science",
  projectId: "shareable-science",
  siteTitle: "Shareable.Science",
  siteDescription: "INSERT SITE DESCRIPTION",
  models: [
    Share,
  ],
};
