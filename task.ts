#!/usr/bin/env -S deno run -A --allow-net --allow-read

import { createWordfresh } from "wordfresh";
import { wordfreshConfig as config } from "./fresh.config.ts";

createWordfresh(config).task(...Deno.args);
