import { useCallback } from "preact/hooks";
import { Reload } from "../components/icons.ts";
import { FunctionComponent } from "preact";

const ReloadButton: FunctionComponent = () => {
  const handleRetry = useCallback(() => {
    globalThis.location.reload();
  }, []);

  return (
    <button
      type="button"
      class="bg-blue text-white px-4 py-2 rounded flex flex-row items-center"
      onClick={handleRetry}
    >
      <Reload />
      <span class="ml-1">Try again?</span>
    </button>
  );
};

export default ReloadButton;
