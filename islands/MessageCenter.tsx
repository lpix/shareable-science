import { signal } from "@preact/signals";
import { FunctionComponent } from "preact";
import { useCallback, useEffect, useRef, useState } from "preact/hooks";

import { HttpError, isHttpError } from "../lib/http.ts";
import { Alert as AlertIcon, Close as CloseIcon } from "../components/icons.ts";

interface Message {
  timestamp: number;
  level: "error";
  message: string;
}

interface Props {
  fromServer?: Omit<Message, "timestamp">;
}

const messageSignal = signal<Message[]>([]);

export const flashError = (
  message: string,
  error?: HttpError | Error | unknown,
) => {
  console.error(error);

  messageSignal.value = [...messageSignal.value, {
    timestamp: Date.now(),
    level: "error",
    message,
  }];
};

const MESSAGE_EXPIRY_TIME = 10 * 1000;

const MessageCenter: FunctionComponent<Props> = ({ fromServer }) => {
  const [expired, setExpired] = useState(0);

  const pageLoadTime = useRef<number>(0);
  useEffect(() => {
    pageLoadTime.current = Date.now();

    return () => {
      pageLoadTime.current = 0;
    };
  }, []);

  const allMessages = fromServer
    ? [
      { ...fromServer, timestamp: pageLoadTime.current },
      ...messageSignal.value,
    ]
    : messageSignal.value;

  useEffect(() => {
    const nMessages = allMessages.length;
    setTimeout(() => {
      if (!pageLoadTime.current) return;
      setExpired(nMessages);
    }, MESSAGE_EXPIRY_TIME);
  }, [allMessages.length]);

  const handleHideMessage = useCallback(() => {
    setExpired(allMessages.length);
  }, [allMessages.length]);

  const lastMessage = allMessages[allMessages.length - 1];
  if (!lastMessage) {
    return null;
  }

  const isExpired = allMessages.length <= expired;

  return (
    <div
      class={`fixed bottom-8 right-8 bg-gray border border-red text-blue px-4 py-2 flex flex-row items-center ${
        isExpired ? "fadable faded" : "fadable"
      }`}
      role="alert"
      aria-live="assertive"
    >
      <button type="button" onClick={handleHideMessage}>
        <CloseIcon />
      </button>
      <span class="ml-2">{lastMessage.message}</span>
      <span class="text-red ml-2">
        <AlertIcon />
      </span>
    </div>
  );
};

export default MessageCenter;
