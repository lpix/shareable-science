import { IS_BROWSER } from "$fresh/runtime.ts";
import { useEffect } from "preact/hooks";

interface Props {
  url: string;
}

const redirect = (url: string) => {
  window.location = url as unknown as Location;
};

export default function (props: Props) {
  useEffect(() => {
    if (!IS_BROWSER) return;

    redirect(props.url);
  });

  return null;
}
