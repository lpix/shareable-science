import Cropper from "../lib/preact-cropper.tsx";
import { useCallback, useRef, useState } from "preact/hooks";

import useCanvas from "../lib/draw.ts";
import { LOADABLE_FONTS } from "../lib/fonts.ts";
import type { Citation, OaLocation, PreviewTextStyle } from "../lib/types.ts";

import { flashError } from "./MessageCenter.tsx";

import StylePanel from "../components/StylePanel.tsx";
import ImageGenerator from "../components/ImageGenerator.tsx";
import { ButtonGroup } from "../components/form.tsx";
import { LoaderSpinner } from "../components/loaders.tsx";

interface Props {
  citation: Citation;
  figureUrls: string[];
  oaLocations: OaLocation[];
  jwt: string;
}

const DEBOUCE = 50;

const DEFAULT_TEXT_STYLE: PreviewTextStyle = {
  font: LOADABLE_FONTS[0],
  opacity: 0.7,
  darkBg: false,
};

export default function ({ figureUrls, citation, oaLocations, jwt }: Props) {
  const [selectedIdx, setSelectedIdx] = useState(0);
  const [isLoadingImg, setIsLoadingImg] = useState(true);
  const handleChangeImage = useCallback((idx: number) => {
    setSelectedIdx(idx);
    setIsLoadingImg(true);
  }, []);

  const [lastCropTime, setLastCropTime] = useState(0);
  const croppedCanvas = useRef<HTMLCanvasElement>(null);
  const handleCrop = useCallback((timestamp: number) => {
    setLastCropTime(timestamp);
  }, []);

  const [textStyle, setTextStyle] = useState<PreviewTextStyle>(
    DEFAULT_TEXT_STYLE,
  );

  const { onRender, getCanvasBlob } = useCanvas(
    citation,
    lastCropTime,
    textStyle,
    croppedCanvas.current,
  );

  return (
    <div class="flex flex-row justify-around items-stretch min-h-[100vh]">
      <div class="h-full py-8 max-w-[600px]">
        <StylePanel currentStyle={textStyle} setStyle={setTextStyle} />
        <canvas
          width={600}
          height={335}
          class="mx-auto"
          style={{
            width: 600,
            height: 335,
            imageRendering: "pixelated",
          }}
          ref={onRender}
        >
        </canvas>
        <ImageGenerator
          getBlob={getCanvasBlob}
          lastCropTime={lastCropTime}
          oaLocations={oaLocations}
          jwt={jwt}
        />
      </div>

      <div class="border-r-2 border-green"></div>

      <div class="py-8 w-[50%]">
        <ButtonGroup
          legend="Select figure"
          value={selectedIdx}
          onChange={handleChangeImage}
          options={figureUrls.map((_url, idx) => ({
            value: idx,
            label: String(idx + 1),
          }))}
        />
        <div
          class={isLoadingImg
            ? "w-full bg-gray relative min-h-[200px]"
            : undefined}
        >
          {isLoadingImg
            ? (
              <div class="absolute inset-0 flex justify-center items-center">
                <LoaderSpinner lightClass="fill-blue" darkClass="fill-gray" />
              </div>
            )
            : null}

          <Cropper
            src={`/api/proxy?src=${figureUrls[selectedIdx]}`}
            debounceInterval={DEBOUCE}
            onUpdate={handleCrop}
            ref={croppedCanvas}
            onLoad={() => setIsLoadingImg(false)}
            onError={(err) => flashError("Unable to load image.", err)}
          />
        </div>
      </div>
    </div>
  );
}
