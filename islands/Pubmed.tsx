import { Fragment, FunctionComponent } from "preact";
import { useCallback, useRef, useState } from "preact/hooks";

import { doSearch, SEARCH_DEBOUNCE } from "../lib/pubmed.ts";
import { useIsMounted } from "../lib/hooks.ts";
import { Clear, Search } from "../components/icons.ts";
import { Button, Input } from "../components/form.tsx";
import { LoaderSpinner } from "../components/loaders.tsx";
import type { Citation } from "../lib/types.ts";
import { Chevron } from "../components/glyphs.tsx";
import { flashError } from "./MessageCenter.tsx";
import { unknown } from "zod";

const INPUT_ID = "lps-node-search";

enum LoadState {
  Ready = "Ready",
  Loading = "Loading",
  Done = "Done",
  Error = "Error",
}

const Pubmed: FunctionComponent = () => {
  const [search, setSearch] = useState("");
  const [load, setLoad] = useState(LoadState.Ready);
  const [results, setResults] = useState<
    {
      count: number;
      papers: Citation[];
    } | null
  >(null);
  const isMounted = useIsMounted();

  const handleSubmit = useCallback((search: string, page?: number) => {
    if (search.length < 3) {
      setResults(null);
      return;
    }

    setLoad(LoadState.Loading);

    doSearch(search, page)
      .then((res) => {
        if (!isMounted) return;
        const data = res.data;
        if (data) {
          setResults((current) => {
            if (current && page && data) {
              return {
                count: res.count,
                papers: [...current.papers, ...data],
              };
            }
            return {
              count: res.count,
              papers: data,
            };
          });

          setLoad(LoadState.Done);
          return;
        }
        throw new Error("NoDataFromNCBI");
      }).catch((err: unknown) => {
        setLoad(LoadState.Error);
        flashError("Unable to reach PubMed", err);
      });
  }, []);

  const handleLoadNextPage = useCallback(() => {
    const nextPage = Math.ceil((results?.papers.length || 0) / 10);
    handleSubmit(search, nextPage);
  }, [results]);

  const timer = useRef<number | undefined>();
  const handleChange = useCallback(
    (search: string) => {
      setSearch(search);
      if (timer.current) {
        clearTimeout(timer.current);
      }

      timer.current = setTimeout(() => {
        handleSubmit(search);
      }, SEARCH_DEBOUNCE);
    },
    [search, handleSubmit],
  );

  return (
    <div>
      <SearchInput
        value={search}
        onInput={handleChange}
        isLoading={load === LoadState.Loading}
        onSubmit={handleSubmit}
      />
      {results && (
        <Results
          count={results.count}
          results={results.papers}
          isLoading={load === LoadState.Loading}
          handleLoadNextPage={handleLoadNextPage}
        />
      )}
    </div>
  );
};

export default Pubmed;

const SearchInput: FunctionComponent<{
  value: string;
  onInput: (next: string) => void;
  isLoading: boolean;
  onSubmit: (search: string) => void;
}> = ({
  value,
  onInput,
  isLoading,
  onSubmit,
}) => {
  const handleSubmit = useCallback(
    (ev: Event) => {
      ev.preventDefault();
      onSubmit(value);
    },
    [onSubmit, value],
  );

  const handleClear = useCallback(() => {
    onInput("");
    onSubmit("");

    document.getElementById(INPUT_ID)?.focus();
  }, [onInput]);

  return (
    <form onSubmit={handleSubmit} className="mb-4">
      <label className="label" htmlFor="search">
        Search PubMed
      </label>
      <Input
        id={INPUT_ID}
        name="search"
        type="text"
        placeholder="e.g. transposons, mcclintok b[author], cold spring harbor[affiliation], etc."
        value={value}
        onInput={onInput}
        autoFocus
        renderLeft={<Search stroke="hsl(73, 96%, 38%)" />}
        renderRight={isLoading ? <LoaderSpinner /> : (
          <button
            className="self-stretch"
            aria-label="Clear input"
            type="button"
            onClick={handleClear}
          >
            <Clear size="2.33rem" stroke="gray" />
          </button>
        )}
      />
    </form>
  );
};

const Results: FunctionComponent<{
  count: number;
  results: Citation[];
  isLoading: boolean;
  handleLoadNextPage: () => void;
}> = ({
  count,
  results,
  isLoading,
  handleLoadNextPage,
}) => {
  if (count === 0) {
    return <h3>No results found</h3>;
  }

  return (
    <Fragment>
      <p className="text-green my-4">
        Total results: <strong>{count}</strong>
      </p>
      <ul className="text-gray-dark my-4">
        {results.map((result) => (
          <li
            key={result.pmid}
            className="my-2"
          >
            <a href={`/edit/${result.pmid}`} class="search-result">
              <div class="flex flex-row items-center">
                <div>
                  <span className="text-blue">{result.authors},</span>
                  <strong>
                    {" "}
                    {result.year}
                    {". "}
                  </strong>
                  <span>"{result.title}"</span>
                </div>
                <div class="w-2 search-result__shift">
                  <Chevron
                    height="1.6rem"
                    className="inline-block pl-2 text-green"
                  />
                </div>
              </div>
            </a>
          </li>
        ))}
      </ul>

      {results.length < count && (
        <div class="my-4 text-center">
          <Button
            onClick={handleLoadNextPage}
            isLoading={isLoading}
          >
            Load more{"\u2026"}
          </Button>
        </div>
      )}
    </Fragment>
  );
};
