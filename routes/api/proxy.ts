import { Handler } from "$fresh/server.ts";
import { z } from "zod";

import { queryParams } from "../../lib/validation.ts";

const parseProxyQS = queryParams(z.object({
  src: z.string(),
}));

export const handler: Handler = async (req: Request) => {
  const qs = parseProxyQS(req.url);
  if (!qs.success) {
    return new Response(JSON.stringify({ ok: false }), { status: 400 });
  }

  const src = decodeURI(qs.data.src);

  const res = await fetch(src);
  if (res.status >= 400) {
    return new Response(JSON.stringify({ ok: false }), { status: 400 });
  }

  return new Response(res.body);
};
