import { Handlers } from "$fresh/server.ts";
import { db } from "wordfresh";

import { verifyJwt } from "../../lib/csrf.ts";
import { respondWithError } from "../../lib/http.ts";
import { Share } from "../../models/Shares.ts";
import { z } from "zod";
import randomId from "../../models/randomId.ts";

export const handler: Handlers = {
  POST: async (req) => {
    const jwt = req.headers.get("x-token");
    if (!jwt) {
      return respondWithError(401);
    }

    const payload = await verifyJwt(jwt);
    if (!payload?.pmid) {
      return respondWithError(401);
    }

    let location: string;
    let imageData: Uint8Array;

    try {
      const form = await req.formData();
      const formLocation = form.get("location");
      z.string().parse(formLocation);
      location = formLocation as string;

      const blob = form.get("image") as Blob;
      const arrayBuffer = await blob.arrayBuffer();
      imageData = new Uint8Array(arrayBuffer);
    } catch (_err) {
      return respondWithError(400);
    }

    try {
      const uid = randomId(10);
      await db.createFile(Share)(
        { pmid: payload.pmid, location, uid },
        "image/png",
        imageData,
      );

      return new Response(JSON.stringify({ ok: true, id: uid }));
    } catch (_err) {
      return respondWithError(503);
    }
  },
};
