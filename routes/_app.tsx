import { type PageProps } from "$fresh/server.ts";
import Logo from "../components/Logo.tsx";

export default function App({ Component }: PageProps) {
  return (
    <html>
      <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>shareable-science</title>
        <link rel="stylesheet" href="/styles.css" />
      </head>
      <body>
        <Component />

        <div class="fixed bottom-0 left-0 bg-blue border-t-2 border-r-2 border-green p-2">
          <a href="https://livingpixel.io" target="_blank">
            <Logo />
          </a>
        </div>
      </body>
    </html>
  );
}
