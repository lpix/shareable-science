import { Head } from "$fresh/runtime.ts";
import Hero from "../components/Hero.tsx";
import SearchBody from "../components/SearchBody.tsx";

export default function Error404() {
  return (
    <>
      <Head>
        <title>404 - Page not found</title>
      </Head>
      <Hero title="Share your Research" />

      <SearchBody>
        <h2 class="py-8 text-h6 text-gray-dark italic font-bold">
          Try searching for a different paper?
        </h2>
      </SearchBody>
    </>
  );
}
