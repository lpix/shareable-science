import { Fragment } from "preact/jsx-runtime";

import Hero from "../components/Hero.tsx";
import SearchBody from "../components/SearchBody.tsx";
import { Head } from "$fresh/runtime.ts";

export default function Home() {
  return (
    <Fragment>
      <Head>
        <title>Shareable.Science</title>

        <meta
          name="description"
          content="A tool to create social media previews for scholarly articles"
        />
        <link rel="shortcut icon" href="/favicon.svg" type="image/x-icon" />

        <meta name="og:type" content="website" />
        <meta name="og:title" content="Shareable.Science" />
        <meta name="og:url" content="https://shareable.science" />
        <meta
          name="og:description"
          content="A tool to create social media previews for scholarly articles"
        />

        <meta name="og:image" content="/app-social-img.png" />
        <meta
          name="og:image:alt"
          content="A tool to create social media previews for scholarly articles"
        />

        <meta name="twitter:card" content="summary_large_image" />
        <meta property="twitter:domain" content="shareable.science" />
        <meta property="twitter:url" content="https://shareable.science" />
        <meta name="twitter:title" content="Shareable.Science" />
        <meta
          name="twitter:description"
          content="A tool to create social media previews for scholarly articles"
        />

        <meta name="twitter:image" content="/app-social-img.png" />
      </Head>

      <Hero title="Shareable.Science" />

      <SearchBody>
        <h2 class="py-8 text-h6 text-gray-dark italic font-bold">
          Create shareable social media previews for your papers
        </h2>
      </SearchBody>
    </Fragment>
  );
}
