import { FreshContext } from "$fresh/server.ts";
import { Head } from "$fresh/runtime.ts";
import { Fragment } from "preact/jsx-runtime";
import { z } from "zod";
import { db, Page } from "wordfresh";

import { Share } from "../../models/Shares.ts";
import { getSummary } from "../../lib/pubmed.ts";
import { getUid } from "../../lib/validation.ts";
import Redirect from "../../islands/Redirect.tsx";

export default async function (req: Request, ctx: FreshContext) {
  const url = new URL(req.url);

  const uid = getUid(ctx);
  if (!uid) {
    return ctx.renderNotFound();
  }

  const data = await db.getRecord(Share)(uid);
  if (!data) {
    return ctx.renderNotFound();
  }

  const citation = await getSummary(data.pmid);
  if (!citation.data) {
    return ctx.renderNotFound();
  }

  const { title, authors } = citation.data;

  return (
    <Fragment>
      <Head>
        <title>{title}</title>

        <meta name="description" content={authors} />
        <meta name="robots" content="noindex" />

        <meta name="og:type" content="website" />
        <meta name="og:title" content={title} />
        <meta name="og:url" content={req.url} />
        <meta name="og:description" content={authors} />

        <meta name="og:image" content={`${url.origin}/image/${uid}`} />
        <meta name="og:image:alt" content={`Figure from ${title}`} />

        <meta name="twitter:card" content="summary_large_image" />
        <meta property="twitter:domain" content={url.origin} />
        <meta property="twitter:url" content={req.url} />
        <meta name="twitter:title" content={title} />
        <meta name="twitter:description" content={authors} />

        <meta name="twitter:image" content={`${url.origin}/image/${uid}`} />
      </Head>

      <h1>{citation.data.title}</h1>
      <img src={`${url.origin}/image/${uid}`} />

      <p>
        You are being redirected. If you are impatient, click{" "}
        <a href={data.location}>here</a>.
      </p>

      <Redirect url={data.location} />
    </Fragment>
  );
}
