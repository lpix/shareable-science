import { Handler, PageProps } from "$fresh/server.ts";
import { z } from "zod";
import { Fragment } from "preact/jsx-runtime";
import { Head } from "$fresh/runtime.ts";

import { createJwt } from "../../lib/csrf.ts";
import { getPaperData } from "../../lib/pubmed.ts";
import { getAllLocations, getOaLocations } from "../../lib/unpaywall.ts";
import { params } from "../../lib/validation.ts";
import type { Citation, OaLocation } from "../../lib/types.ts";
import SearchBody from "../../components/SearchBody.tsx";
import MessageCenter from "../../islands/MessageCenter.tsx";
import ReloadButton from "../../islands/ReloadButton.tsx";
import PreviewEditor from "../../islands/PreviewEditor.tsx";

const parser = params(z.object({
  pmid: z.coerce.number(),
}));

interface Props {
  figureUrls: string[];
  citation?: Citation;
  oaLocations: OaLocation[];
  jwt?: string;
}

export const handler: Handler<Props> = async (_req, ctx) => {
  const params = parser(ctx.params);
  if (!params.success) {
    return ctx.render({
      figureUrls: [],
      oaLocations: [],
    });
  }

  try {
    const { pmid } = params.data;
    const jwt = await createJwt(pmid);
    const { citation, figureUrls } = await getPaperData(pmid);
    const oaLocations = citation.doi
      ? (await getOaLocations(citation.doi))
      : [];

    return ctx.render({
      citation,
      figureUrls,
      oaLocations,
      jwt,
    });
  } catch (e) {
    return ctx.render({
      figureUrls: [],
      oaLocations: [],
    });
  }
};

export default function ({ data }: PageProps<Props>) {
  if (!data.citation) {
    return (
      <SearchBody>
        <h2 class="py-8 text-h6 text-red italic font-bold">
          Unable to find citation information for this paper.
        </h2>
        <div class="flex flex-row justify-center mb-6">
          <ReloadButton />
        </div>
        <p>Or try a different paper:</p>
      </SearchBody>
    );
  }

  if (!data.jwt) {
    return (
      <SearchBody>
        <h2 class="py-8 text-h6 text-red italic font-bold">
          A server error occurred.
        </h2>
        <div class="flex flex-row justify-center mb-6">
          <ReloadButton />
        </div>
      </SearchBody>
    );
  }

  const locations = getAllLocations(data.citation, data.oaLocations);

  return (
    <Fragment>
      <Head>
        <link
          rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.6.1/cropper.min.css"
        />
      </Head>
      <PreviewEditor
        figureUrls={data.figureUrls}
        citation={data.citation}
        oaLocations={locations}
        jwt={data.jwt}
      />
      <MessageCenter
        fromServer={data.figureUrls.length
          ? undefined
          : {
            message: "No figures could be retrieved for this paper",
            level: "error",
          }}
      />
    </Fragment>
  );
}
