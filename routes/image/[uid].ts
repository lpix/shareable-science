import { Handler } from "$fresh/server.ts";
import { db } from "wordfresh";

import { Share } from "../../models/Shares.ts";
import { respondWithError } from "../../lib/http.ts";
import { getUid } from "../../lib/validation.ts";

export const handler: Handler = async (_req, ctx) => {
  const uid = getUid(ctx);
  if (!uid) {
    return respondWithError(400);
  }

  const res = await db.getFile(Share)(uid);
  if (res.status < 400) {
    return res;
  }

  return respondWithError(res.status);
};
