import { FunctionComponent } from "preact";
import type { Citation } from "../lib/types.ts";
import useCanvas from "../lib/draw.ts";
import { forwardRef, MutableRefObject } from "preact/compat";

interface Props {
  updateTime: number;
  citation: Citation;
}

const Preview: FunctionComponent<Props> = forwardRef<HTMLCanvasElement, Props>(
  ({ citation, updateTime }, ref) => {
    const _ref = ref as MutableRefObject<HTMLCanvasElement | null>;

    const onRender = useCanvas(citation, updateTime, _ref.current);

    return (
      <canvas
        width={600}
        height={335}
        style={{
          width: 600,
          height: 335,
          imageRendering: "pixelated",
        }}
        ref={onRender}
      >
      </canvas>
    );
  },
);

export default Preview;
