import { useCallback, useState } from "preact/hooks";
import { Fragment, FunctionComponent, JSX } from "preact";
import { LoaderSpinner } from "./loaders.tsx";
import { useStableIds } from "../lib/hooks.ts";
import { Clear as ClearIcon, Search as SearchIcon } from "./icons.ts";

export const Input: FunctionComponent<{
  id?: string;
  name?: string;
  type?: "text";
  placeholder?: string;
  autoFocus?: boolean;
  list?: string;
  value: string;
  onInput: (nextValue: string) => void;
  renderLeft?: JSX.Element;
  renderRight?: JSX.Element;
}> = ({ onInput, renderLeft, renderRight, ...props }) => {
  const handleInput = useCallback((ev: Event) => {
    ev.preventDefault();
    const el = ev.target as HTMLInputElement;
    onInput(el.value);
  }, []);

  return (
    <div className="w-full flex flex-row items-center border border-gray-dark rounded focus-within:outline outline-blue-outline outline-offset-2 px-2">
      {renderLeft || null}
      <input
        className="text-blackish flex-grow text-h6 px-2 py-2 outline-none"
        {...props}
        onInput={handleInput}
      />
      {renderRight || null}
    </div>
  );
};

export const RadioSet: FunctionComponent<{
  legend: string;
  value: string;
  onChange: (next: string) => void;
  options: Array<{ value: string; label: string | JSX.Element }>;
}> = ({ legend, value, onChange, options }) => {
  const current = value;

  const [name] = useStableIds();

  const handleChange = useCallback(
    (ev: Event) => {
      const value = (ev.target as HTMLInputElement).value;
      onChange(value);
    },
    [onChange],
  );

  return (
    <fieldset>
      <legend className="font-semi">{legend}</legend>
      {options.map(({ value, label }) => (
        <label key={value} className="block">
          <input
            type="radio"
            name={name}
            value={value}
            className="mr-1"
            checked={current === value}
            onChange={handleChange}
          />
          <span className="text-gray-dark">{label}</span>
        </label>
      ))}
    </fieldset>
  );
};

export const Select = <
  T extends string | number,
>({ label, value, onChange, options }: {
  label: string;
  value: T;
  onChange: (next: T) => void;
  options: Array<{ value: T; label: string }>;
}) => {
  const [id] = useStableIds(1);

  const handleChange = useCallback((ev: Event) => {
    const value = (ev.target as HTMLSelectElement).value as T;
    onChange(value);
  }, [onChange]);

  return (
    <div>
      <label htmlFor={id}>{label}</label>
      <select
        value={value}
        onChange={handleChange}
        id={id}
        className="mt-1 py-1 px-2"
      >
        {options.map(({ value, label }) => (
          <option value={value} key={value}>{label}</option>
        ))}
      </select>
    </div>
  );
};

export const SearchInput: FunctionComponent<{
  value: string;
  onInput: (nextValue: string) => void;
  label: string;
  onClear?: () => void;
  name?: string;
  list?: string;
  autoFocus?: boolean;
}> = (props) => {
  const [inputId] = useStableIds(1);

  return (
    <div>
      <label className="label" htmlFor={inputId}>
        {props.label}
      </label>
      <Input
        id={inputId}
        name="search"
        type="text"
        placeholder={`Search\u2026`}
        renderLeft={<SearchIcon stroke="hsl(73, 96%, 38%)" />}
        renderRight={props.value && props.onClear
          ? (
            <button
              className="self-stretch"
              aria-label="Clear input"
              type="button"
              onClick={props.onClear}
            >
              <ClearIcon size="2.33rem" stroke="gray" />
            </button>
          )
          : undefined}
        {...props}
      />
    </div>
  );
};

export const ComboBox = <
  T extends string | number,
>({ label, onSelect, options }: {
  label: string;
  onSelect: (next: T) => void;
  options: Array<{ value: T; label: string }>;
}) => {
  const [listId] = useStableIds(1);
  const [inputValue, setInputValue] = useState("");

  const handleClear = useCallback(() => {
    setInputValue("");
  }, []);

  const handleChange = useCallback((label: string) => {
    const selected = options.find((option) => option.label === label);
    if (selected) {
      onSelect(selected.value);
      handleClear();
      return;
    }

    setInputValue(label);
  }, [onSelect]);

  return (
    <Fragment>
      <SearchInput
        value={inputValue}
        onInput={handleChange}
        label={label}
        list={listId}
      />
      <datalist id={listId}>
        {options.map((option) => (
          <option key={option.value} value={option.label} />
        ))}
      </datalist>
    </Fragment>
  );
};

export const Button: FunctionComponent<{
  isLoading: boolean;
  onClick: () => void;
}> = ({ isLoading, children, onClick }) => {
  return (
    <button
      type="button"
      onClick={onClick}
      className="bg-green hover:bg-green-alt rounded text-white py-2 px-4 relative"
      disabled={isLoading}
    >
      <span className={isLoading ? "invisible" : ""}>{children}</span>
      {isLoading
        ? (
          <div className="absolute inset-0 flex items-center justify-center">
            <LoaderSpinner lightClass="fill-white" />
          </div>
        )
        : null}
    </button>
  );
};

export const ButtonGroup = <T extends string | number>(
  { legend, value, onChange, options }: {
    legend: string;
    value: T;
    onChange: (next: T) => void;
    options: Array<{ value: T; label: string | JSX.Element }>;
  },
) => {
  const current = value;

  return (
    <fieldset class="button-group">
      <legend className="font-semi">{legend}</legend>
      <ul class="button-group__ul">
        {options.map(({ value, label }) => (
          <li key={value} class="button-group__li">
            <button
              type="button"
              onClick={() =>
                onChange(value)}
              aria-pressed={value === current ? "true" : undefined}
              class="button-group__button"
            >
              {label}
            </button>
          </li>
        ))}
      </ul>
    </fieldset>
  );
};
