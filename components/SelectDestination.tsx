import { FunctionComponent } from "preact";
import { useStableIds } from "../lib/hooks.ts";
import type { OaLocation } from "../lib/types.ts";

interface Props {
  selectedLocation: OaLocation;
  oaLocations: OaLocation[];
  setSelectedLocation: (next: OaLocation) => void;
}

const SelectDestination: FunctionComponent<Props> = (
  { selectedLocation, oaLocations, setSelectedLocation },
) => {
  const handleChange = (ev: Event) => {
    const url = (ev.target as HTMLSelectElement).value;
    const next = oaLocations.find((location) => location.url === url);
    if (!next) return;

    setSelectedLocation(next);
  };
  const [id] = useStableIds(1);

  return (
    <div class="mb-2">
      <label htmlFor={id} class="mr-2">Select destination</label>
      <select
        id={id}
        className="mt-1 py-1 px-2 rounded border border-blue max-w-full"
        name="destination"
        value={selectedLocation.url}
        onInput={handleChange}
      >
        {oaLocations.map((location) => (
          <option key={location.url} value={location.url}>
            {location.label || location.url}
          </option>
        ))}
      </select>
    </div>
  );
};

export default SelectDestination;
