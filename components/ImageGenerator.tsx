import { useCallback, useEffect, useState } from "preact/hooks";
import { FunctionComponent } from "preact";

import { OaLocation } from "../lib/types.ts";
import { flashError } from "../islands/MessageCenter.tsx";

import CopyableText from "./CopyableText.tsx";
import SelectDestination from "./SelectDestination.tsx";
import { Link as LinkIcon } from "./icons.ts";
import { Button } from "./form.tsx";

interface Props {
  getBlob: () => Promise<Blob>;
  lastCropTime: number;
  oaLocations: OaLocation[];
  jwt: string;
}

enum Stages {
  Ready,
  Loading,
  Error,
  Done,
}

const ImageGenerator: FunctionComponent<Props> = (
  { getBlob, oaLocations, lastCropTime, jwt },
) => {
  const [stage, setStage] = useState(Stages.Ready);
  const [targetUrl, setTargetUrl] = useState("");
  useEffect(() => {
    setStage(Stages.Ready);
    setTargetUrl("");
  }, [lastCropTime]);

  const [selectedLocation, setSelectedLocation] = useState(oaLocations[0]);
  const handleChangeSelectedLocation = useCallback((location: OaLocation) => {
    setSelectedLocation(location);
    setStage(Stages.Ready);
    setTargetUrl("");
  }, []);

  const handleSubmit = useCallback((ev?: Event) => {
    ev?.preventDefault();
    setStage(Stages.Loading);

    getBlob().then((blob) => {
      const formData = new FormData();
      formData.append("location", selectedLocation.url);
      formData.append("image", blob);

      return fetch("/api/submit", {
        method: "POST",
        body: formData,
        headers: {
          "x-token": jwt,
        },
      });
    })
      .then((res) => {
        return res.json();
      }).then((data) => {
        if (!data || !data.ok) {
          throw new Error("ApiBadResponse");
        }

        setStage(Stages.Done);
        setTargetUrl(`${globalThis.location.origin}/s/${data.id}`);
      }).catch((err) => {
        setStage(Stages.Error);
        flashError("Error generating preview page", err);
      });
  }, [lastCropTime, selectedLocation]);

  return (
    <form onSubmit={handleSubmit} class="my-4">
      <SelectDestination
        selectedLocation={selectedLocation}
        setSelectedLocation={handleChangeSelectedLocation}
        oaLocations={oaLocations}
      />
      <div class="my-4 text-center">
        {stage === Stages.Done && targetUrl
          ? (
            <div class="my-8">
              <h3 class="text-h3 text-green font-bold">
                Your social media share link is:
              </h3>
              <CopyableText text={targetUrl} />
            </div>
          )
          : (
            <Button onClick={handleSubmit} isLoading={stage === Stages.Loading}>
              <LinkIcon />
              <span class="ml-1">Generate URL</span>
            </Button>
          )}
      </div>
    </form>
  );
};

export default ImageGenerator;
