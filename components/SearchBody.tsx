import { Fragment, FunctionComponent } from "preact";
import Pubmed from "../islands/Pubmed.tsx";
import MessageCenter from "../islands/MessageCenter.tsx";

const SearchBody: FunctionComponent = ({ children }) => {
  return (
    <Fragment>
      <div class="max-w-[768px] mx-auto pb-8">
        {children}
        <Pubmed />
      </div>

      <MessageCenter />
    </Fragment>
  );
};

export default SearchBody;
