import { useCallback, useState } from "preact/hooks";
import { FunctionComponent } from "preact";
import { useIsMounted } from "../lib/hooks.ts";
import { ClipboardCheck, ClipboardCopy } from "./icons.ts";

interface Props {
  text: string;
}

const TIME_TO_RESET = 10 * 1000; // 10 sec

const CopyableText: FunctionComponent<Props> = ({ text }) => {
  const [isCopied, setIsCopied] = useState(false);
  const isMounted = useIsMounted();
  const handleCopy = useCallback((ev: MouseEvent) => {
    ev.preventDefault();
    navigator.clipboard.writeText(text);
    setIsCopied(true);

    setTimeout(() => {
      isMounted && setIsCopied(false);
    }, TIME_TO_RESET);
  }, [text]);

  return (
    <button
      type="button"
      onClick={handleCopy}
      class="flex flex-row justify-center items-center mx-auto"
    >
      <span
        class={`border border-gray-dark px-2 py-1
        ${isCopied ? "bg-blue text-white" : "bg-gray"}`}
      >
        {text}
      </span>
      <span class="text-blue">
        {isCopied
          ? <ClipboardCheck size="1.6rem" />
          : <ClipboardCopy size="1.6rem" />}
      </span>
    </button>
  );
};

export default CopyableText;
