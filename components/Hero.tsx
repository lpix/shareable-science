import { FunctionComponent } from "preact";

interface Props {
  title: string;
}

const Hero: FunctionComponent<Props> = (
  { title },
) => {
  return (
    <header className="bg-gray-dark pr-6 py-12 md:py-16">
      <div className="lps-crossline">
        <h1 className="inline-block text-h1/8 text-white py-6 pl-6 pr-12 md:pl-20 border-y-[4px] border-green lps-br-dot lps-tr-dot lps-center-line">
          <span className="inline-block sm:px-4">{title}</span>
        </h1>
      </div>
    </header>
  );
};

export default Hero;
