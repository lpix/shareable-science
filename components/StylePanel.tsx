import { FunctionComponent } from "preact";
import { DarkMode } from "./icons.ts";
import { LOADABLE_FONTS } from "../lib/fonts.ts";

import type { PreviewTextStyle } from "../lib/types.ts";

interface Props {
  currentStyle: PreviewTextStyle;
  setStyle: (next: PreviewTextStyle) => void;
}

const StylePanel: FunctionComponent<Props> = ({ currentStyle, setStyle }) => {
  const handleChangeFont = (ev: Event) => {
    const value = (ev.target as HTMLSelectElement).value;
    const font = LOADABLE_FONTS.find((font) => font.value === value);
    if (!font) return;

    setStyle({
      ...currentStyle,
      font,
    });
  };

  const handleChangeDarkBg = (ev: Event) => {
    setStyle({
      ...currentStyle,
      darkBg: !currentStyle.darkBg,
    });
  };

  const handleChangeOpacity = (ev: Event) => {
    const value = Number((ev.target as HTMLInputElement).value);
    setStyle({
      ...currentStyle,
      opacity: value,
    });
  };

  return (
    <form class="flex flex-row justify-start items-center mb-2">
      <div class="mr-4">
        <select
          class="py-1 px-2 rounded border border-blue"
          value={currentStyle.font.value}
          onChange={handleChangeFont}
        >
          {LOADABLE_FONTS.map(({ value, label }) => (
            <option value={value}>{label}</option>
          ))}
        </select>
      </div>
      <div class="mr-4">
        <button
          type="button"
          class={` p-1 rounded flex ${
            currentStyle.darkBg ? "bg-gray text-blue" : "bg-blue text-white"
          }`}
          aria-pressed={currentStyle.darkBg ? "true" : undefined}
          onClick={handleChangeDarkBg}
        >
          <DarkMode />
        </button>
      </div>
      <div>
        <input
          type="range"
          value={currentStyle.opacity}
          onChange={handleChangeOpacity}
          min="0"
          max="1"
          step="0.01"
        />
      </div>
    </form>
  );
};

export default StylePanel;
