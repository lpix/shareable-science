FROM denoland/deno

ENV DENO_DEPLOYMENT_ID="alpha2023dec20"

ENV RUST_BACKTRACE=1

EXPOSE 8000

WORKDIR /app

ADD . /app

RUN deno cache --lock-write main.ts

CMD ["run", "-A", "main.ts"]
