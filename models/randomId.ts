const CHARS = "abcdefghijklmnopqrstuvwxyz0123456789";

const randomId = (len: number) => {
  let s = "";
  for (let lp1 = 0; lp1 < len; lp1++) {
    s += CHARS.charAt(Math.floor(Math.random() * CHARS.length));
  }
  return s;
};

export default randomId;
