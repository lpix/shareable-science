import { Model } from "wordfresh";
import { z } from "zod";

export const ShareSchema = z.object({
  uid: z.string(),
  pmid: z.number().gt(0),
  location: z.string(),
});

export type TShareSchema = z.infer<typeof ShareSchema>;

export const Share: Model<TShareSchema> = {
  modelName: "share-item",

  schema: ShareSchema,

  getId: ({ uid }) => uid,
};
