# Shareable.Science

[Shareable.Science](https://shareable.science/) is a tool to create social media previews for scholarly articles. It currently works by searching the MEDLINE database at National Centre for Biotechnology Information (NCBI), but may be extended to other literature database in the future (if there is interest).

Shareable.Science works by allowing the user to edit an image from their figures and citation data, of the approximate dimensions used on social media and chat applications. It then generates a "webpage" containing the required [OpenGraph](https://ogp.me/) and [Twitter card](https://developer.twitter.com/en/docs/twitter-for-websites/cards/overview/abouts-cards) metadata. Although these metadata standards were originally developed by Facebook and Twitter, respectively, they also work to "preview" links that are copy-pasted to a wide variety of other platforms. The generated page then redirects (human) traffic to the paper itself.

## Who made it?

Shareable.Science is developed by [Living Pixel Solutions Ltd.](https://livingpixel.io/). It is free to use.

## Bugs and feature requests

Please use the issue tracker (https://gitlab.com/lpix/shareable-science/-/issues) to report a bug or request a feature. You will need to create a GitLab account.

## Tech

Shareable.Science is written in Deno and uses a CouchDB database. If you'd like to contribute code, please file an issue requesting that I find the time to write set-up instructions.