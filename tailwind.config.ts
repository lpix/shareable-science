import { type Config } from "tailwindcss";

export default {
  content: [
    "{routes,islands,components}/**/*.{ts,tsx}",
  ],
  theme: {
    extend: {
      zIndex: {
        "modal": "100",
        "popover": "10",
        "up": "1",
      },
    },
    container: {
      center: true,
    },
    colors: {
      green: {
        DEFAULT: "hsl(73, 96%, 38%)",
        alt: "hsl(73, 96%, 33%)",
      },
      blue: {
        DEFAULT: "hsl(190, 60%, 19%)",
        alt: "hsl(190, 40%, 24%)",
        "overlay": "hsla(190, 60%, 19%, 0.2)",
        outline: "hsla(190, 60%, 19%, 0.6)",
      },
      "gray-dark": "hsl(188, 12%, 36%)",
      "gray": "hsl(180, 5%, 89%)",
      red: "hsl(0, 100%, 50%)",
      white: "#fff",
      blackish: "hsl(0, 0%, 20%)",
      overlay: "hsl(180, 5%, 89%, 0.95)",
    },
    fontFamily: {
      sans: ["Rubik", "sans-serif"],
      mono: ['"Courier New"', "Courier", "monospace"],
    },
    fontSize: {
      base: "20px",
      h1: "1.75rem",
      h2: "1.6rem",
      h3: "1.55rem",
      h4: "1.45rem",
      h5: "1.45rem",
      h6: "1.35rem",
      sm: "0.85rem",
    },
  },
} satisfies Config;
