#!/usr/bin/env -S deno run -A --watch=static/,routes/

import dev from "$fresh/dev.ts";
import config, { wordfreshConfig } from "./fresh.config.ts";

import "$std/dotenv/load.ts";

import { createWordfresh } from "wordfresh";

createWordfresh(wordfreshConfig);
await dev(import.meta.url, "./main.ts", config);
